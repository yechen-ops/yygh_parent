package cn.yechen.yygh.cmn.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author yechen
 * @create 2021-08-18 11:36
 */
@FeignClient("service-cmn")// 需要调用的服务名称
@Repository
public interface DictFeignClient {

    @GetMapping("/admin/cmn/dict/getName/{dictcode}/{value}")
    String getDictName(@PathVariable("dictcode") String dictcode, @PathVariable("value") String value);

    @GetMapping("/admin/cmn/dict/getName/{value}")
    String getDictName(@PathVariable("value") String value);
}

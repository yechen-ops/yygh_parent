package cn.yechen.yygh.oss.controller;

import cn.yechen.yygh.common.result.Result;
import cn.yechen.yygh.oss.service.FileService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author yechen
 * @create 2021-08-21 20:22
 */
@Api(tags = "阿里云oss文件上传接口")
@RestController
@RequestMapping("api/oss/file")
public class FileApiController {

    @Autowired
    private FileService fileService;

    // 上传文件到阿里云
    @ApiOperation("上传文件到阿里云")
    @PostMapping("/fileUpload")
    public Result fileUpload(MultipartFile file) {
        // 获取上传的文件
        String fileURL = fileService.fileUpload(file);
        return Result.ok(fileURL);
    }
}

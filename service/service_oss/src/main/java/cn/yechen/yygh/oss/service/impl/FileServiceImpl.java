package cn.yechen.yygh.oss.service.impl;

import cn.yechen.yygh.oss.service.FileService;
import cn.yechen.yygh.oss.utils.ConstantOssPropertiesUtils;
import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import org.joda.time.DateTime;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

/**
 * @author yechen
 * @create 2021-08-21 20:25
 */
@Service
public class FileServiceImpl implements FileService {

    @Override
    public String fileUpload(MultipartFile multipartFile) {

        // yourEndpoint填写Bucket所在地域对应的Endpoint。以华东1（杭州）为例，Endpoint填写为https://oss-cn-hangzhou.aliyuncs.com。
        String endpoint = ConstantOssPropertiesUtils.EDNPOINT;
        // 阿里云账号AccessKey拥有所有API的访问权限，风险很高。强烈建议您创建并使用RAM用户进行API访问或日常运维，请登录RAM控制台创建RAM用户。
        String accessKeyId = ConstantOssPropertiesUtils.ACCESS_KEY_ID;
        String accessKeySecret = ConstantOssPropertiesUtils.SECRECT;
        String bucket = ConstantOssPropertiesUtils.BUCKET;

        // 创建OSSClient实例。
        OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);

        // 填写本地文件的完整路径。如果未指定本地路径，则默认从示例程序所属项目对应本地路径中上传文件流。
        InputStream inputStream = null;
        try {
            inputStream = multipartFile.getInputStream();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // 文件名称
        String fileName = UUID.randomUUID().toString().replace("-", "") + multipartFile.getOriginalFilename();
        // 文件夹名称
        String path = new DateTime().toString("yyyy/MM/dd");
        // 文件路径
        String filePath = path + "/" + fileName;

        // 依次填写Bucket名称（例如examplebucket）和Object完整路径（例如exampledir/exampleobject.txt）。Object完整路径中不能包含Bucket名称。
        ossClient.putObject(bucket, filePath, inputStream);

        // 关闭OSSClient。
        ossClient.shutdown();

        // 文件上传之后的文件路径
        // "https://yygh-yechen.oss-cn-hangzhou.aliyuncs.com/"
        return "https://" + bucket + "." + endpoint + "/" + filePath;
    }
}

package cn.yechen.yygh.oss.service;

import org.springframework.web.multipart.MultipartFile;

/**
 * @author yechen
 * @create 2021-08-21 20:25
 */
public interface FileService {
    String fileUpload(MultipartFile multipartFile);

}

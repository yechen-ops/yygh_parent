package cn.yechen.yygh.hosp.service;

import cn.yechen.yygh.model.hosp.HospitalSet;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @author yechen
 * @create 2021-08-14 15:38
 */
public interface HospitalSetService extends IService<HospitalSet> {
    String getSignKeyByHoscode(String hoscode);
}

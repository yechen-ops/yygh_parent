package cn.yechen.yygh.hosp.service;

import cn.yechen.yygh.model.hosp.Schedule;
import cn.yechen.yygh.vo.hosp.ScheduleQueryVo;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Map;

/**
 * @author yechen
 * @create 2021-08-17 20:31
 */
public interface ScheduleService {
    void saveSchedule(Map<String, Object> parameterMap);

    Page<Schedule> findPageSchedule(int page, int limit, ScheduleQueryVo scheduleQueryVo);

    void removeSchedule(String hoscode, String hosScheduleId);

    Map<String, Object> getScheduleRule(Long page, Long limit, String hoscode, String depcode);

    List<Schedule> getDetailSchedule(String hoscode, String depcode, String workDate);
}

package cn.yechen.yygh.hosp.controller.api;

import cn.yechen.yygh.common.exception.YyghException;
import cn.yechen.yygh.common.helper.HttpRequestHelper;
import cn.yechen.yygh.common.result.Result;
import cn.yechen.yygh.common.result.ResultCodeEnum;
import cn.yechen.yygh.common.utils.MD5;
import cn.yechen.yygh.hosp.service.DepartmentService;
import cn.yechen.yygh.hosp.service.HospitalService;
import cn.yechen.yygh.hosp.service.HospitalSetService;
import cn.yechen.yygh.hosp.service.ScheduleService;
import cn.yechen.yygh.model.hosp.Department;
import cn.yechen.yygh.model.hosp.Hospital;
import cn.yechen.yygh.model.hosp.Schedule;
import cn.yechen.yygh.vo.hosp.DepartmentQueryVo;
import cn.yechen.yygh.vo.hosp.DepartmentVo;
import cn.yechen.yygh.vo.hosp.ScheduleQueryVo;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.sql.ResultSet;
import java.util.Map;

/**
 * @author yechen
 * @create 2021-08-17 11:44
 */
@Slf4j
@Api(tags = "医院方调用接口")
@RestController
@RequestMapping("/api/hosp")
public class ApiController {
    @Autowired
    private HospitalService hospitalService;

    @Autowired
    private HospitalSetService hospitalSetService;

    @Autowired
    private DepartmentService departmentService;

    @Autowired
    private ScheduleService scheduleService;

    // 判断签名是否一致
    private boolean judgeSignKey(Map<String, Object> parameterMap) {
        // 获取医院系统传递过来的签名（经过 MD5 加密的）
        String sign = (String) parameterMap.get("sign");
        // log.info("传递的签名："+sign);
        // 根据传递过来的医院编号查询签名
        String hoscode = (String) parameterMap.get("hoscode");
        // 校验医院编号
        if(StringUtils.isEmpty(hoscode)) {
            throw new YyghException(ResultCodeEnum.PARAM_ERROR);
        }

        String signKey = hospitalSetService.getSignKeyByHoscode(hoscode);
        // log.info("获取的签名："+signKey);
        String signKeyMd5 = MD5.encrypt(signKey);
        // log.info("加密后的签名："+signKeyMd5);
        return sign.equals(signKeyMd5);
    }

    // 获取参数 map 对象
    private Map<String, Object> getParameterMap(HttpServletRequest request) {
        // 获取传递过来的医院信息
        Map<String, String[]> httpParameterMap = request.getParameterMap();
        Map<String, Object> parameterMap = HttpRequestHelper.switchMap(httpParameterMap);

        // 判断签名
        boolean flag = judgeSignKey(parameterMap);
        // 两个加密过后的签名比较，不一致就抛异常
        if (!flag) {
            throw new YyghException(ResultCodeEnum.SIGN_ERROR);
        }
        return parameterMap;
    }

    // 转换 base64 中的 “ ” 为 “+”
    private void replaceBase64(Map<String, Object> parameterMap) {
        String logoDataString = (String)parameterMap.get("logoData");
        if(!StringUtils.isEmpty(logoDataString)) {
            String logoData = logoDataString.replaceAll(" ", "+");
            parameterMap.put("logoData", logoData);
        }
    }

    // 接受医院数据的接口
    @PostMapping("/saveHospital")
    public Result saveHospital(HttpServletRequest request) {
        Map<String, Object> parameterMap = getParameterMap(request);

        replaceBase64(parameterMap);
        // 调用 service 的方法
        hospitalService.save(parameterMap);
        return Result.ok();

    }

    // 查询医院数据接口
    @PostMapping("/hospital/show")
    public Result getHospital(HttpServletRequest request) {
        Map<String, Object> parameterMap = getParameterMap(request);
        String hoscode = (String)parameterMap.get("hoscode");

        // 调用 service 的方法
        Hospital hospital = hospitalService.getHospitalByHoscode(hoscode);
        return Result.ok(hospital);
    }

    // 接受医院部门信息接口
    @PostMapping("/saveDepartment")
    public Result saveDepartment(HttpServletRequest request) {
        Map<String, Object> parameterMap = getParameterMap(request);
        // 调用 service
        departmentService.saveDepartment(parameterMap);
        return Result.ok();
    }

    // 查询科室数据接口
    @PostMapping("/department/list")
    public Result getDepartmentList(HttpServletRequest request) {
        Map<String, Object> parameterMap = getParameterMap(request);
        // 获取信息
        String hoscode = (String)parameterMap.get("hoscode");
        int page = Integer.parseInt((String) parameterMap.get("page"));
        int limit = Integer.parseInt((String) parameterMap.get("limit"));
        page = StringUtils.isEmpty(page) ? 1 : page;
        limit = StringUtils.isEmpty(limit) ? 1 : limit;

        // 调用 service 方法
        Page<Department> departmentPage =  departmentService.findPageDepartment(page, limit, hoscode);
        return Result.ok(departmentPage);
    }

    // 删除科室信息接口
    @PostMapping("/department/remove")
    public Result removeDepartment(HttpServletRequest request) {
        Map<String, Object> parameterMap = getParameterMap(request);
        // 获取医院编号
        String hoscode = (String)parameterMap.get("hoscode");
        // 获取部门编号
        String depcode = (String)parameterMap.get("depcode");
        // 调用 service
        departmentService.removeDepartment(hoscode, depcode);
        return Result.ok();
    }

    // 接受医院排班信息接口
    @PostMapping("/saveSchedule")
    public Result saveSchedule(HttpServletRequest request) {
        Map<String, Object> parameterMap = getParameterMap(request);

        // 保存排班信息
        scheduleService.saveSchedule(parameterMap);
        return Result.ok();
    }

    // 查询排班信息接口
    @PostMapping("schedule/list")
    public Result getScheduleList(HttpServletRequest request) {
        Map<String, Object> parameterMap = getParameterMap(request);
        // 获取参数
        String hoscode = (String)parameterMap.get("hoscode");
        String depcode = (String)parameterMap.get("depcode");
        int page = StringUtils.isEmpty(parameterMap.get("page")) ? 1 : Integer.parseInt((String)parameterMap.get("page"));
        int limit = StringUtils.isEmpty(parameterMap.get("limit")) ? 10 : Integer.parseInt((String)parameterMap.get("limit"));

        ScheduleQueryVo scheduleQueryVo = new ScheduleQueryVo();
        scheduleQueryVo.setHoscode(hoscode);
        scheduleQueryVo.setDepcode(depcode);
        Page<Schedule> pageModel = scheduleService.findPageSchedule(page , limit, scheduleQueryVo);
        return Result.ok(pageModel);
    }

    // 删除排班信息接口
    @PostMapping("/schedule/remove")
    public Result removeSchedule(HttpServletRequest request) {
        Map<String, Object> parameterMap = getParameterMap(request);
        // 获取参数
        String hoscode = (String)parameterMap.get("hoscode");
        String hosScheduleId = (String)parameterMap.get("hosScheduleId");

        // 删除操作
        scheduleService.removeSchedule(hoscode, hosScheduleId);
        return Result.ok();
    }


}

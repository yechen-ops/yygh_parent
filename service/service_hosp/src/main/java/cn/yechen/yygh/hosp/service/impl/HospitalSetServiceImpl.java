package cn.yechen.yygh.hosp.service.impl;

import cn.yechen.yygh.hosp.mapper.HospitalSetMapper;
import cn.yechen.yygh.hosp.service.HospitalSetService;
import cn.yechen.yygh.model.hosp.HospitalSet;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * @author yechen
 * @create 2021-08-14 15:39
 */
@Service
public class HospitalSetServiceImpl extends ServiceImpl<HospitalSetMapper, HospitalSet> implements HospitalSetService {

    @Override
    public String getSignKeyByHoscode(String hoscode) {
        QueryWrapper<HospitalSet> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("hoscode", hoscode);
        HospitalSet hospitalSet = baseMapper.selectOne(queryWrapper);
        return  hospitalSet.getSignKey();
    }
}

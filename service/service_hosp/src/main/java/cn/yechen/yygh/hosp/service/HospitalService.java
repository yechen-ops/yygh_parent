package cn.yechen.yygh.hosp.service;

import cn.yechen.yygh.model.hosp.Hospital;
import cn.yechen.yygh.vo.hosp.HospitalQueryVo;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Map;

/**
 * @author yechen
 * @create 2021-08-17 11:41
 */
public interface HospitalService {
    void save(Map<String, Object> parameterMap);

    Hospital getHospitalByHoscode(String hoscode);

    Page<Hospital> hospitalPage(Integer page, Integer limit, HospitalQueryVo queryVo);

    void updateHospStatus(String id, Integer status);

    Map<String, Object> getHospitalDetail(String id);

    List<Hospital> getHospitalByHosname(String hosname);

    Map<String, Object> getHospitalDetailByHoscode(String hoscode);
}

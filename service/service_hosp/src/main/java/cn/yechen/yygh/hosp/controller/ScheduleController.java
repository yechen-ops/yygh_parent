package cn.yechen.yygh.hosp.controller;

import cn.yechen.yygh.common.result.Result;
import cn.yechen.yygh.hosp.service.ScheduleService;
import cn.yechen.yygh.model.hosp.Schedule;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @author yechen
 * @create 2021-08-18 21:45
 */
@Api(tags = "排班信息管理")
@RestController
@RequestMapping("/admin/hosp/schedule")
// @CrossOrigin// 解决跨域问题（统一使用 gateway 处理跨域问题）
public class ScheduleController {
    @Autowired
    private ScheduleService scheduleService;

    // 通过科室编号和医院编号查询排班规则
    @ApiOperation("通过科室编号和医院编号查询排班规则")
    @GetMapping("/getScheduleRule/{page}/{limit}/{hoscode}/{depcode}")
    public Result getScheduleRule(@PathVariable Long page,
                                  @PathVariable Long limit,
                                  @PathVariable String hoscode,
                                  @PathVariable String depcode) {
        Map<String, Object> scheduleRuleMap = scheduleService.getScheduleRule(page, limit, hoscode, depcode);
        return Result.ok(scheduleRuleMap);
    }

    // 获取详细排班信息
    @ApiOperation("获取详细排班信息")
    @GetMapping("getDetailSchedule/{hoscode}/{depcode}/{workDate}")
    public Result getDetailSchedule(@PathVariable String hoscode, @PathVariable String depcode, @PathVariable String workDate) {
        List<Schedule> scheduleList = scheduleService.getDetailSchedule(hoscode, depcode, workDate);
        return Result.ok(scheduleList);
    }
}

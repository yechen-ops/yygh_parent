package cn.yechen.yygh.hosp.mapper;

import cn.yechen.yygh.model.hosp.HospitalSet;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @author yechen
 * @create 2021-08-14 15:35
 */
public interface HospitalSetMapper extends BaseMapper<HospitalSet> {

}

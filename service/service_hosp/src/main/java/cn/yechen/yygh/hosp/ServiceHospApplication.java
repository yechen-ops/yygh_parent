package cn.yechen.yygh.hosp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author yechen
 * @create 2021-08-14 15:16
 */
@SpringBootApplication
@ComponentScan(basePackages = "cn.yechen")
@EnableDiscoveryClient// 启用发现客户端，使用 Nacos
@EnableFeignClients(basePackages = "cn.yechen")// 启用 Feign 客户端，注入调用的服务
public class ServiceHospApplication {
    public static void main(String[] args) {
        SpringApplication.run(ServiceHospApplication.class, args);
    }
}

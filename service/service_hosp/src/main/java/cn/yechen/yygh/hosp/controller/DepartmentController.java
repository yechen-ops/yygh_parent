package cn.yechen.yygh.hosp.controller;

import cn.yechen.yygh.common.result.Result;
import cn.yechen.yygh.hosp.service.DepartmentService;
import cn.yechen.yygh.model.hosp.Department;
import cn.yechen.yygh.vo.hosp.DepartmentVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author yechen
 * @create 2021-08-18 20:08
 */
@Api(tags = "科室管理")
@RestController
@RequestMapping("/admin/hosp/department")
// @CrossOrigin// 解决跨域问题（统一使用 gateway 处理跨域问题）
public class DepartmentController {

    @Autowired
    private DepartmentService departmentService;

    // 根据医院编号查询医院所有科室列表
    @ApiOperation("根据医院编号查询医院所有科室列表")
    @GetMapping("/getDeptList/{hoscode}")
    public Result getDeptList(@PathVariable String hoscode) {
        List<DepartmentVo> departmentVoList =  departmentService.getDepList(hoscode);
        return Result.ok(departmentVoList);
    }
}

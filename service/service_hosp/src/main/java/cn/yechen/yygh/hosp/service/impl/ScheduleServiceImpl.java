package cn.yechen.yygh.hosp.service.impl;

import cn.yechen.yygh.hosp.repository.DepartmentRepository;
import cn.yechen.yygh.hosp.repository.ScheduleRepository;
import cn.yechen.yygh.hosp.service.HospitalService;
import cn.yechen.yygh.hosp.service.ScheduleService;
import cn.yechen.yygh.model.hosp.Schedule;
import cn.yechen.yygh.vo.hosp.BookingScheduleRuleVo;
import cn.yechen.yygh.vo.hosp.ScheduleQueryVo;
import com.alibaba.fastjson.JSONObject;
import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.*;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author yechen
 * @create 2021-08-17 20:31
 */
@Service
public class ScheduleServiceImpl implements ScheduleService {

    @Autowired
    private ScheduleRepository scheduleRepository;

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private HospitalService hospitalService;

    @Autowired
    private DepartmentRepository departmentRepository;

    @Override
    public void saveSchedule(Map<String, Object> parameterMap) {
        // 将 map 转为 Schedule 对象
        Schedule schedule = JSONObject.parseObject(JSONObject.toJSONString(parameterMap), Schedule.class);
        // 通过 hoscode 和 hosScheduleId 查询排班是否存在
        Schedule scheduleExit = scheduleRepository.getScheduleByHoscodeAndHosScheduleId(schedule.getHoscode(), schedule.getHosScheduleId());


        if(null == scheduleExit) {
            // 排班信息不存在，保存操作
            schedule.setCreateTime(new Date());
        } else {
            //  排班信息存在，更新操作
            schedule.setId(scheduleExit.getId());
            schedule.setCreateTime(scheduleExit.getCreateTime());
            schedule.setUpdateTime(new Date());
        }
        schedule.setIsDeleted(0);
        scheduleRepository.save(schedule);
    }

    @Override
    public Page<Schedule> findPageSchedule(int page, int limit, ScheduleQueryVo scheduleQueryVo) {
        // 排序（按创建时间降序排）
        Sort sort = Sort.by(Sort.Direction.DESC, "createTime");
        // 分页
        Pageable pageable = PageRequest.of(page-1, limit, sort);
        // 查询条件
        Schedule schedule = new Schedule();
        BeanUtils.copyProperties(scheduleQueryVo, schedule);
        schedule.setIsDeleted(0);
        //创建匹配器，即如何使用查询条件
        ExampleMatcher matcher = ExampleMatcher.matching() //构建对象
                .withStringMatcher(ExampleMatcher.StringMatcher.CONTAINING) //改变默认字符串匹配方式：模糊查询
                .withIgnoreCase(true); //改变默认大小写忽略方式：忽略大小写
        //创建实例
        Example<Schedule> example = Example.of(schedule, matcher);
        Page<Schedule> pages = scheduleRepository.findAll(example, pageable);
        return pages;
    }

    @Override
    public void removeSchedule(String hoscode, String hosScheduleId) {
        // 判断是否存在
        Schedule schedule = scheduleRepository.getScheduleByHoscodeAndHosScheduleId(hoscode, hosScheduleId);
        // 如果存在，则删除
        if (schedule != null) {
            scheduleRepository.deleteById(schedule.getId());
        }
    }

    @Override
    public Map<String, Object> getScheduleRule(Long page, Long limit, String hoscode, String depcode) {
        // 根据医院编号和科室编号封装查询条件
        Criteria criteria = Criteria.where("hoscode").is(hoscode).and("depcode").is(depcode);
        // 匹配条件
        MatchOperation match = Aggregation.match(criteria);
        // 分组字段
        GroupOperation groupOperation = Aggregation.group("workDate").first("workDate").as("workDate")
            // 统计号源数量
            .count().as("docCount")
                    .sum("reservedNumber").as("reservedNumber")
                    .sum("availableNumber").as("availableNumber");
        // 排序
        SortOperation sort = Aggregation.sort(Sort.Direction.ASC, "workDate");
        // 实现分页
        SkipOperation skip = Aggregation.skip((page - 1) * limit);
        LimitOperation limit1 = Aggregation.limit(limit);

        // 根据工作如期（workDate）进行分组
        Aggregation aggregation = Aggregation.newAggregation(match, groupOperation, sort, skip, limit1);

        // 调用方法，执行查询操作
        AggregationResults<BookingScheduleRuleVo> aggregate = mongoTemplate.aggregate(aggregation, Schedule.class, BookingScheduleRuleVo.class);
        // 获取查询 list 结果
        List<BookingScheduleRuleVo> bookingScheduleRuleVoList = aggregate.getMappedResults();

        // 获取分组查询的总记录数
        Aggregation totalAgg = Aggregation.newAggregation(Aggregation.match(criteria), Aggregation.group("workDate"));
        int total = mongoTemplate.aggregate(totalAgg, Schedule.class, BookingScheduleRuleVo.class).getMappedResults().size();

        // 获取日期对应的星期
        for (BookingScheduleRuleVo bookingScheduleRuleVo : bookingScheduleRuleVoList) {
            Date workDate = bookingScheduleRuleVo.getWorkDate();
            String dayOfWeek = this.getDayOfWeek(new DateTime(workDate));
            bookingScheduleRuleVo.setDayOfWeek(dayOfWeek);
        }

        // 获取医院名称
        String hosname = hospitalService.getHospitalByHoscode(hoscode).getHosname();

        // 最终返回数据
        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("bookingScheduleRuleVoList", bookingScheduleRuleVoList);
        resultMap.put("total", total);
        Map<String, Object> baseMap = new HashMap<>();
        baseMap.put("hosname", hosname);
        resultMap.put("baseMap", baseMap);

        return resultMap;
    }

    @Override
    public List<Schedule> getDetailSchedule(String hoscode, String depcode, String workDate) {
        List<Schedule> scheduleList = scheduleRepository.getScheduleByHoscodeAndDepcodeAndWorkDate(hoscode, depcode, new DateTime(workDate).toDate());

        // 设置其他值
        Map<String, Object> map = new HashMap<>();
        // 医院名称
        map.put("hosname", hospitalService.getHospitalByHoscode(hoscode).getHosname());
        // 科室名称
        map.put("depname", departmentRepository.getDepartmentByHoscodeAndDepcode(hoscode, depcode).getDepname());
        // 日期对应星期
        map.put("dayOfWeek", this.getDayOfWeek(new DateTime(workDate)));
        for (Schedule schedule : scheduleList) {
            schedule.setParam(map);
        }

        return scheduleList;
    }

    /**
     * 根据日期获取周几数据
     * @param dateTime
     * @return
     */
    private String getDayOfWeek(DateTime dateTime) {
        String dayOfWeek = "";
        switch (dateTime.getDayOfWeek()) {
            case DateTimeConstants.SUNDAY:
                dayOfWeek = "周日";
                break;
            case DateTimeConstants.MONDAY:
                dayOfWeek = "周一";
                break;
            case DateTimeConstants.TUESDAY:
                dayOfWeek = "周二";
                break;
            case DateTimeConstants.WEDNESDAY:
                dayOfWeek = "周三";
                break;
            case DateTimeConstants.THURSDAY:
                dayOfWeek = "周四";
                break;
            case DateTimeConstants.FRIDAY:
                dayOfWeek = "周五";
                break;
            case DateTimeConstants.SATURDAY:
                dayOfWeek = "周六";
            default:
                break;
        }
        return dayOfWeek;
    }
}

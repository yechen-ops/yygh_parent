package cn.yechen.yygh.hosp.service.impl;

import cn.yechen.yygh.cmn.client.DictFeignClient;
import cn.yechen.yygh.hosp.repository.HospitalRepository;
import cn.yechen.yygh.hosp.service.HospitalService;
import cn.yechen.yygh.hosp.service.HospitalSetService;
import cn.yechen.yygh.model.hosp.Hospital;
import cn.yechen.yygh.model.hosp.HospitalSet;
import cn.yechen.yygh.vo.hosp.HospitalQueryVo;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.apache.logging.log4j.util.Strings;
import org.springframework.data.domain.Page;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author yechen
 * @create 2021-08-17 11:41
 */
@Service
public class HospitalServiceImpl implements HospitalService {

    @Autowired
    private HospitalRepository hospitalRepository;

    // 注入远程服务
    @Autowired
    private DictFeignClient dictFeignClient;

    @Override
    public void save(Map<String, Object> parameterMap) {
        // 将传递过来的 map 集合转换为对象 Hospital
        String jsonString = JSONObject.toJSONString(parameterMap);
        Hospital hospital = JSONObject.parseObject(jsonString, Hospital.class);

        // 判断是否存在相同数据
        String hoscode = hospital.getHoscode();
        Hospital hospitalExit = hospitalRepository.getHospitalByHoscode(hoscode);

        // 如果不存在，添加
        if (hospitalExit == null) {
            hospital.setStatus(0);
            hospital.setCreateTime(new Date());
        } else {
            // 如果存在，更新
            hospital.setId(hospitalExit.getId());
            hospital.setStatus(hospitalExit.getStatus());
            hospital.setCreateTime(hospitalExit.getCreateTime());
            hospital.setUpdateTime(new Date());
        }
        hospital.setIsDeleted(0);
        hospitalRepository.save(hospital);
    }

    @Override
    public Hospital getHospitalByHoscode(String hoscode) {
        Hospital result = hospitalRepository.getHospitalByHoscode(hoscode);
        return result;
    }

    @Override
    public Page<Hospital> hospitalPage(Integer page, Integer limit, HospitalQueryVo queryVo) {
        // 分页
        Pageable pageable = PageRequest.of(page-1, limit);
        // 条件匹配器
        ExampleMatcher exampleMatcher = ExampleMatcher.matching()
                .withStringMatcher(ExampleMatcher.StringMatcher.CONTAINING)
                .withIgnoreCase();
        // 查询条件
        Hospital hospital = new Hospital();
        BeanUtils.copyProperties(queryVo, hospital);
        Example<Hospital> example = Example.of(hospital, exampleMatcher);

        // 执行分页查询
        Page<Hospital> hospitalPage = hospitalRepository.findAll(example, pageable);

        // 遍历医院集合，为每个 Hospital 赋上医院类型名称字段
        for (Hospital h : hospitalPage.getContent()) {
            this.setHospitalParam(h);
        }

        return hospitalPage;
    }

    @Override
    public void updateHospStatus(String id, Integer status) {
        Hospital hospital = hospitalRepository.findById(id).get();
        // 设置修改的值
        hospital.setStatus(status);
        hospital.setUpdateTime(new Date());
        // 保存
        hospitalRepository.save(hospital);
    }

    @Override
    public Map<String, Object> getHospitalDetail(String id) {
        Hospital hospital = hospitalRepository.findById(id).get();
        this.setHospitalParam(hospital);
        Map<String, Object> map = new HashMap<>();
        // 单独将医院等级取出返回
        map.put("bookingRule", hospital.getBookingRule());
        hospital.setBookingRule(null);
        map.put("hospital", hospital);

        return map;
    }

    @Override
    public List<Hospital> getHospitalByHosname(String hosname) {
        return hospitalRepository.findHospitalByHosnameLike(hosname);
    }

    @Override
    public Map<String, Object> getHospitalDetailByHoscode(String hoscode) {
        // 查询医院详情
        Hospital hospital = getHospitalByHoscode(hoscode);
        setHospitalParam(hospital);

        Map<String, Object> map = new HashMap<>();
        // 单独将医院等级取出返回
        map.put("bookingRule", hospital.getBookingRule());
        hospital.setBookingRule(null);
        map.put("hospital", hospital);

        return map;
    }


    private void setHospitalParam(Hospital hospital) {
        // 调用 servcie-cmn 模块提供的服务获取名称
        String hostypeStr = dictFeignClient.getDictName("HospitalType", hospital.getHostype());
        String provinceStr = dictFeignClient.getDictName(hospital.getProvinceCode());
        String cityStr = dictFeignClient.getDictName(hospital.getCityCode());
        String districtStr = dictFeignClient.getDictName(hospital.getDistrictCode());

        // 赋值
        hospital.getParam().put("hostypeStr", hostypeStr);
        hospital.getParam().put("fullAddress", provinceStr+cityStr+districtStr);
    }

}

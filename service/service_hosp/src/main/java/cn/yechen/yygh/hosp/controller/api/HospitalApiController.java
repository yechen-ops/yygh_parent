package cn.yechen.yygh.hosp.controller.api;

import cn.yechen.yygh.common.result.Result;
import cn.yechen.yygh.hosp.service.DepartmentService;
import cn.yechen.yygh.hosp.service.HospitalService;
import cn.yechen.yygh.model.hosp.Hospital;
import cn.yechen.yygh.vo.hosp.DepartmentVo;
import cn.yechen.yygh.vo.hosp.HospitalQueryVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * @author yechen
 * @create 2021-08-19 19:42
 */
@Api(tags = "医院前台接口")
@RestController
@RequestMapping("/api/hosp/hospital")
public class HospitalApiController {
    @Autowired
    private HospitalService hospitalService;

    @Autowired
    private DepartmentService departmentService;

    // 查询医院列表
    @ApiOperation("查询医院列表")
    @GetMapping("/getHospitalList/{page}/{limit}")
    public Result getHospitalList(@PathVariable Integer page, @PathVariable Integer limit, HospitalQueryVo queryVo) {
        Page<Hospital> hospitals = hospitalService.hospitalPage(page, limit, queryVo);
        return Result.ok(hospitals);
    }

    // 根据医院名称进行查询
    @ApiOperation("根据医院名称进行模糊查询")
    @GetMapping("/getHospitalByHosname/{hosname}")
    public  Result getHospitalByHosname(@PathVariable String hosname) {
        List<Hospital> hospitalList = hospitalService.getHospitalByHosname(hosname);
        return Result.ok(hospitalList);
    }

    // 根据医院编号获取科室信息
    @ApiOperation("根据医院编号获取科室信息")
    @GetMapping("/getDepartment/{hoscode}")
    public Result getDepartmentByHoscode(@PathVariable String hoscode) {
        List<DepartmentVo> depList = departmentService.getDepList(hoscode);
        return Result.ok(depList);
    }

    // 根据医院编号获取医院的详细信息
    @ApiOperation("根据医院编号获取医院的详细信息")
    @GetMapping("/getHospDetail/{hoscode}")
    public Result getHospDetail(@PathVariable String hoscode) {
        Map<String, Object> map = hospitalService.getHospitalDetailByHoscode(hoscode);
        return Result.ok(map);
    }
}

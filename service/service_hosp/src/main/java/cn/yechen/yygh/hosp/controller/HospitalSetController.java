package cn.yechen.yygh.hosp.controller;

import cn.yechen.yygh.common.exception.YyghException;
import cn.yechen.yygh.common.result.ResultCodeEnum;
import cn.yechen.yygh.common.utils.MD5;
import cn.yechen.yygh.hosp.service.HospitalSetService;
import cn.yechen.yygh.model.hosp.HospitalSet;
import cn.yechen.yygh.common.result.Result;
import cn.yechen.yygh.vo.hosp.HospitalSetQueryVo;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Random;

/**
 * @author yechen
 * @create 2021-08-14 15:43
 */
@Api(tags = "医院设置管理")
@RestController
@RequestMapping("/admin/hosp/hospitalSet")
// @CrossOrigin// 解决跨域问题（统一使用 gateway 处理跨域问题）
public class HospitalSetController {
    // 注入 service
    @Autowired
    private HospitalSetService hospitalSetService;

    // 查询医院设置表的所有信息
    @ApiOperation("获取所有的医院设置")
    @GetMapping("/all")
    public Result findAllHospitalSet() {
        List<HospitalSet> list = hospitalSetService.list();
        return Result.ok(list);
    }

    // 删除医院（逻辑删除）
    @ApiOperation("通过id删除医院（逻辑删除）")
    @DeleteMapping("/remove/{id}")
    public Result removeHospitalSet(@PathVariable Long id) {
        boolean flag = hospitalSetService.removeById(id);
        if (flag) {
            return Result.ok();
        } else {
            return Result.fail();
        }
    }

    // 条件查询带分页
    @ApiOperation("条件查询带分页")
    @PostMapping("/page/{current}/{limit}")
    public Result findPageHospitalSet(@PathVariable int current, @PathVariable int limit, @RequestBody(required = false) HospitalSetQueryVo hospitalSetQueryVo) {
        Page<HospitalSet> page = new Page<>(current, limit);
        // 查询条件
        QueryWrapper<HospitalSet> queryWrapper = new QueryWrapper<>();
        // 判断查询条件是否存在
        if (!Strings.isEmpty(hospitalSetQueryVo.getHosname())) {
            queryWrapper.like("hosname", hospitalSetQueryVo.getHosname());
        }
        if (!Strings.isEmpty(hospitalSetQueryVo.getHoscode())) {
            queryWrapper.eq("hoscode", hospitalSetQueryVo.getHoscode());
        }
        queryWrapper.orderByDesc("create_time");
        // 条件分页查询
        Page<HospitalSet> hospitalSetPage = hospitalSetService.page(page, queryWrapper);
        // 返回结果
        return Result.ok(hospitalSetPage);
    }

    // 添加医院
    @ApiOperation("添加医院")
    @PostMapping("/save")
    public Result saveHospitalSet(@RequestBody HospitalSet hospitalSet) {
        // 设置一个 签名秘钥
        Random random = new Random();
        hospitalSet.setSignKey(MD5.encrypt(System.currentTimeMillis()+""+random.nextInt(1000)));
        boolean flag = hospitalSetService.save(hospitalSet);
        if (flag) {
            return Result.ok();
        } else {
            return Result.fail();
        }
    }

    // 根据 id 获取医院
    @ApiOperation("根据 id 获取医院")
    @GetMapping("/getById/{id}")
    public Result getHospitalSetById(@PathVariable long id) {
        // 模拟一个异常
        /*try {
            int i = 10 / 0;
        } catch (Exception e) {
            throw new YyghException(ResultCodeEnum.FAIL);
        }*/
        HospitalSet hospitalSet = hospitalSetService.getById(id);
        return Result.ok(hospitalSet);
    }

    // 修改医院设置
    @ApiOperation("修改医院设置")
    @PutMapping("/update")
    public Result updateHospitalSet(@RequestBody HospitalSet hospitalSet) {
        boolean flag = hospitalSetService.updateById(hospitalSet);
        if (flag) {
            return Result.ok();
        } else {
            return Result.fail();
        }
    }

    // 批量删除医院
    @ApiOperation("批量删除医院（逻辑删除）")
    @DeleteMapping("/batchRemove")
    public Result batchRemoveHospitalSet(@RequestBody List<Long> idList) {
        boolean flag = hospitalSetService.removeByIds(idList);
        if (flag) {
            return Result.ok();
        } else {
            return Result.fail();
        }
    }

    // 医院设置锁定和解锁
    @ApiOperation("医院设置锁定和解锁")
    @PutMapping("/lock/{id}/{status}")
    public Result lockHospitalSet(@PathVariable Long id, @PathVariable Integer status) {
        // 根据 id 查询医院信息
        HospitalSet hospitalSet = hospitalSetService.getById(id);
        // 修改 status 的值
        hospitalSet.setStatus(status);
        // 更新医院信息
        boolean flag = hospitalSetService.updateById(hospitalSet);
        if (flag) {
            return Result.ok();
        } else {
            return Result.fail();
        }
    }

    // 发送签名密钥
    @ApiOperation("发送签名密钥")
    @PutMapping("/sendKey/{id}")
    public Result sendKey(@PathVariable Long id) {
        HospitalSet hospitalSet = hospitalSetService.getById(id);
        String signKey = hospitalSet.getSignKey();
        String hoscode = hospitalSet.getHoscode();
        //TODO 发送短信
        return Result.ok();
    }



}

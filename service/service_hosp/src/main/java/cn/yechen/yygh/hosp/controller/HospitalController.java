package cn.yechen.yygh.hosp.controller;

import cn.yechen.yygh.common.result.Result;
import cn.yechen.yygh.hosp.service.HospitalService;
import cn.yechen.yygh.model.hosp.Hospital;
import cn.yechen.yygh.vo.hosp.HospitalQueryVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @author yechen
 * @create 2021-08-18 10:07
 */
@Api(tags = "医院管理")
@RestController
@RequestMapping("/admin/hosp/hospital")
// @CrossOrigin// 解决跨域问题（统一使用 gateway 处理跨域问题）
public class HospitalController {
    @Autowired
    private HospitalService hospitalService;

    // 条件查询带分页
    @ApiOperation("条件查询带分页")
    @GetMapping("/page/{page}/{limit}")
    public Result hospitalPage(@PathVariable Integer page, @PathVariable Integer limit, HospitalQueryVo queryVo) {
        Page<Hospital> hospitalsPage = hospitalService.hospitalPage(page, limit, queryVo);
        return Result.ok(hospitalsPage);
    }

    // 更新医院的上线状态
    @ApiOperation("更新医院的上线状态")
    @PutMapping("/updateHospStatus/{id}/{status}")
    public Result updateHospStatus(@PathVariable String id, @PathVariable Integer status) {
        hospitalService.updateHospStatus(id, status);
        return Result.ok();
    }

    // 获取医院的详细信息
    @ApiOperation("获取医院的详细信息")
    @GetMapping("/showHospitalDetail/{id}")
    public Result showHospitalDetail(@PathVariable String id) {
        Map<String, Object> hospitalDetail = hospitalService.getHospitalDetail(id);
        return Result.ok(hospitalDetail);

    }

}

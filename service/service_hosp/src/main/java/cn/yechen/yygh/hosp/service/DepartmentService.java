package cn.yechen.yygh.hosp.service;

import cn.yechen.yygh.model.hosp.Department;
import cn.yechen.yygh.vo.hosp.DepartmentQueryVo;
import cn.yechen.yygh.vo.hosp.DepartmentVo;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Map;

/**
 * @author yechen
 * @create 2021-08-17 16:30
 */
public interface DepartmentService {
    void saveDepartment(Map<String, Object> parameterMap);

    Page<Department> findPageDepartment(int page, int limit, String hoscode);


    void removeDepartment(String hoscode, String depcode);

    List<DepartmentVo> getDepList(String hoscode);
}

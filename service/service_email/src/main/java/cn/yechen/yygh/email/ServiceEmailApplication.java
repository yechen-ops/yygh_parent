package cn.yechen.yygh.email;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author yechen
 * @create 2021-08-21 10:05
 */
// 取消数据源自动配置
@SpringBootApplication
@ComponentScan(basePackages = "cn.yechen")
@EnableDiscoveryClient
public class ServiceEmailApplication {
    public static void main(String[] args) {
        SpringApplication.run(ServiceEmailApplication.class, args);
    }
}

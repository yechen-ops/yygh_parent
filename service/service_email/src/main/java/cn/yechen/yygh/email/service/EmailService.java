package cn.yechen.yygh.email.service;

/**
 * @author yechen
 * @create 2021-08-21 13:04
 */
public interface EmailService {
    boolean sendCode(String phone, String code);
}

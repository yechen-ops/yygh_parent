package cn.yechne.easyexcel;

import com.alibaba.excel.EasyExcel;

import java.util.ArrayList;
import java.util.List;

/**
 * @author yechen
 * @create 2021-08-16 12:24
 */
public class TestWrite {
    public static void main(String[] args) {
        // 设置 excel 文件路径和名称
        String fileName = "C:\\Users\\30117\\Desktop\\excel\\user01.xlsx";

        // 准备数据
        List<UserData> list = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            list.add(new UserData(i, "username"+i));
        }

        // 调用方法
        EasyExcel.write(fileName, UserData.class).sheet("用户信息").doWrite(list);
    }
}

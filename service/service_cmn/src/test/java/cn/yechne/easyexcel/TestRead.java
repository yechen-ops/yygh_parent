package cn.yechne.easyexcel;

import com.alibaba.excel.EasyExcel;

/**
 * @author yechen
 * @create 2021-08-16 12:37
 */
public class TestRead {
    public static void main(String[] args) {
        // 读取文件路径和名称
        String fileName = "C:\\Users\\30117\\Desktop\\excel\\user01.xlsx";

        // 调用方法
        EasyExcel.read(fileName, UserData.class, new ExcelListener()).sheet("用户信息").doRead();
    }
}

package cn.yechen.yygh.cmn.service;

import cn.yechen.yygh.model.cmn.Dict;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author yechen
 * @create 2021-08-16 10:30
 */
public interface DictService extends IService<Dict> {

    List<Dict> findChildData(Long id);

    void exportDictData(HttpServletResponse response);

    void importDictData(MultipartFile file);

    String getDictName(String dictcode, String value);

    List<Dict> findByDictCode(String dictCode);

    List<Dict> findByParentId(Long parentId);
}

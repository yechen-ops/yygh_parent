package cn.yechen.yygh.cmn.controller;

import cn.yechen.yygh.cmn.service.DictService;
import cn.yechen.yygh.common.result.Result;
import cn.yechen.yygh.model.cmn.Dict;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author yechen
 * @create 2021-08-16 10:33
 */
@Api(tags = "数据字典接口")
@RestController
@RequestMapping("/admin/cmn/dict")
// @CrossOrigin// 解决跨域问题（统一使用 gateway 处理跨域问题）
public class DictController {
    @Autowired
    private DictService dictService;

    // 根据数据的 id 查询子数据列表
    @ApiOperation("根据数据的 id 查询子数据列表")
    @GetMapping("/findChileData/{id}")
    public Result fidChileData(@PathVariable Long id) {
        List<Dict> dictList = dictService.findChildData(id);
        return Result.ok(dictList);
    }

    // 导出数据字典列表到 excel
    @ApiOperation("导出数据字典列表")
    @GetMapping("/exportData")
    public void exportDictData(HttpServletResponse response) {
        dictService.exportDictData(response);
    }

    // 导入字典数据
    @ApiOperation("导入字典数据")
    @PostMapping("/importData")
    public Result importDictData(MultipartFile file) {
        dictService.importDictData(file);
        return Result.ok();
    }

    // 根据 value 进行查询
    @ApiOperation("根据 value 查询字典值")
    @GetMapping("/getName/{value}")
    public String getDictName(@PathVariable String value) {
        String dictName = dictService.getDictName("", value);
        return dictName;
    }

    // 根据 dictcode 和 value 进行查询（解决 value 可能重复的问题）
    @ApiOperation("根据 dictcode 和 value 进行查询（解决 value 可能重复的问题）")
    @GetMapping("/getName/{dictcode}/{value}")
    public String getDictName(@PathVariable String dictcode, @PathVariable String value) {
        String dictName = dictService.getDictName(dictcode, value);
        return dictName;
    }

    // 根据 dictCode 获取下级节点
    @ApiOperation(value = "根据 dictCode 获取下级节点")
    @GetMapping("/findByDictCode/{dictCode}")
    public Result<List<Dict>> findByDictCode(@ApiParam(name = "dictCode", value = "节点编码", required = true) @PathVariable String dictCode) {
        List<Dict> list = dictService.findByDictCode(dictCode);
        return Result.ok(list);
    }

    // 根据 parentId 获取子数据
    @ApiOperation("根据 parentId 获取子数据")
    @GetMapping("/findByParentId/{parentId}")
    public Result<List<Dict>> findByParentId(@ApiParam(name = "parentId", value = "父id", required = true) @PathVariable String parentId) {
        List<Dict> dictList = dictService.findByParentId(Long.parseLong(parentId));
        return Result.ok(dictList);
    }

}

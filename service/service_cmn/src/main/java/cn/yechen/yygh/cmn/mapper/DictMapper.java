package cn.yechen.yygh.cmn.mapper;

import cn.yechen.yygh.model.cmn.Dict;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @author yechen
 * @create 2021-08-16 10:29
 */
public interface DictMapper extends BaseMapper<Dict> {

}

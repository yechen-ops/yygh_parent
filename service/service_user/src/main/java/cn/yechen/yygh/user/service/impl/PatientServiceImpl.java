package cn.yechen.yygh.user.service.impl;

import cn.yechen.yygh.cmn.client.DictFeignClient;
import cn.yechen.yygh.enums.DictEnum;
import cn.yechen.yygh.model.user.Patient;
import cn.yechen.yygh.user.mapper.PatientMapper;
import cn.yechen.yygh.user.service.PatientService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author yechen
 * @create 2021-08-22 11:35
 */
@Service
public class PatientServiceImpl extends ServiceImpl<PatientMapper, Patient> implements PatientService {

    @Autowired
    private DictFeignClient dictFeignClient;

    @Override
    public boolean savePatient(Patient patient) {
        int insert = baseMapper.insert(patient);
        return insert == 1;
    }

    @Override
    public List<Patient> findAllPatientByUserId(Long userId) {
        QueryWrapper<Patient> queryWrapper = new QueryWrapper();
        queryWrapper.eq("user_id", userId);
        List<Patient> patientList = baseMapper.selectList(queryWrapper);

        // 将 patient 中的一些值通过查询数据字典替换
        patientList.forEach(this::packPatient);

        return patientList;
    }

    @Override
    public Patient getPatientByPatientId(Long patientId) {
        Patient patient = baseMapper.selectById(patientId);
        this.packPatient(patient);
        return patient;
    }

    @Override
    public boolean updatePatient(Patient patient) {
        int update = baseMapper.updateById(patient);
        return update == 1;
    }

    @Override
    public boolean removePatient(Long patientId) {
        QueryWrapper queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("id", patientId);
        int delete = baseMapper.delete(queryWrapper);
        return delete == 1;
    }

    // Patient 对象里面其他参数封装
    private void packPatient(Patient patient) {
        // 远程调用获取
        // 就诊人证件类型
        String certificatesTypeString = dictFeignClient.getDictName(DictEnum.CERTIFICATES_TYPE.getDictCode(), patient.getCertificatesType());
        // 联系人证件类型
        String contactsCertificatesTypeString = dictFeignClient.getDictName(DictEnum.CERTIFICATES_TYPE.getDictCode(),patient.getContactsCertificatesType());
        // 省
        String provinceString = dictFeignClient.getDictName(patient.getProvinceCode());
        // 市
        String cityString = dictFeignClient.getDictName(patient.getCityCode());
        // 区
        String districtString = dictFeignClient.getDictName(patient.getDistrictCode());

        // 将数据保存至 patient 对象的 param 属性中
        patient.getParam().put("certificatesTypeString", certificatesTypeString);
        patient.getParam().put("contactsCertificatesTypeString", contactsCertificatesTypeString);
        patient.getParam().put("provinceString", provinceString);
        patient.getParam().put("cityString", cityString);
        patient.getParam().put("districtString", districtString);
        patient.getParam().put("fullAddress", provinceString + cityString + districtString + patient.getAddress());
    }
}

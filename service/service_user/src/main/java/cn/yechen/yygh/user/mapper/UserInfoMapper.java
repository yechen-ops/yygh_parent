package cn.yechen.yygh.user.mapper;

import cn.yechen.yygh.model.user.UserInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @author yechen
 * @create 2021-08-20 17:54
 */
public interface UserInfoMapper extends BaseMapper<UserInfo> {
}

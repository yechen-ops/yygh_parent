package cn.yechen.yygh.user.service;

import cn.yechen.yygh.model.user.Patient;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @author yechen
 * @create 2021-08-22 11:35
 */
public interface PatientService extends IService<Patient> {

    boolean savePatient(Patient patient);

    List<Patient> findAllPatientByUserId(Long userId);

    Patient getPatientByPatientId(Long patientId);


    boolean updatePatient(Patient patient);

    boolean removePatient(Long patientId);

}

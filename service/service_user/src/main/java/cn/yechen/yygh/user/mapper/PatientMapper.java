package cn.yechen.yygh.user.mapper;

import cn.yechen.yygh.model.user.Patient;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @author yechen
 * @create 2021-08-22 11:33
 */
public interface PatientMapper extends BaseMapper<Patient> {
}

package cn.yechen.yygh.user.service.impl;

import cn.yechen.yygh.common.exception.YyghException;
import cn.yechen.yygh.common.helper.JwtHelper;
import cn.yechen.yygh.common.result.ResultCodeEnum;
import cn.yechen.yygh.enums.AuthStatusEnum;
import cn.yechen.yygh.model.user.Patient;
import cn.yechen.yygh.user.mapper.UserInfoMapper;
import cn.yechen.yygh.model.user.UserInfo;
import cn.yechen.yygh.user.service.PatientService;
import cn.yechen.yygh.user.service.UserInfoService;
import cn.yechen.yygh.vo.user.LoginVo;
import cn.yechen.yygh.vo.user.UserAuthVo;
import cn.yechen.yygh.vo.user.UserInfoQueryVo;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author yechen
 * @create 2021-08-20 17:53
 */

@Service
public class UserInfoServiceImpl extends ServiceImpl<UserInfoMapper, UserInfo> implements UserInfoService {

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @Autowired
    private PatientService patientService;

    @Override
    public Map<String, Object> loginByPhone(LoginVo loginVo) {
        // 校验参数（手机号 + 验证码）
        String phone = loginVo.getPhone();
        String code = loginVo.getCode();
        if (StringUtils.isEmpty(phone) || StringUtils.isEmpty(code)) {
            // 抛异常（参数不正确）
            throw new YyghException(ResultCodeEnum.PARAM_ERROR);
        }

        // 校验验证码是否正确
        String codeInRedis = redisTemplate.opsForValue().get(phone);
        if (!code.equals(codeInRedis)) {
            throw new YyghException(ResultCodeEnum.CODE_ERROR);
        }

        // 查询当前手机号是否已经注册了
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("phone", phone);
        UserInfo userInfo = baseMapper.selectOne(queryWrapper);

        // 未注册，就注册
        if (null == userInfo) {
            userInfo = new UserInfo();
            userInfo.setName("");
            userInfo.setPhone(phone);
            userInfo.setStatus(1);
            baseMapper.insert(userInfo);
        }

        // 校验当前用户是否被禁用
        if (userInfo.getStatus() == 0) {
            throw new YyghException(ResultCodeEnum.LOGIN_DISABLED_ERROR);
        }

        // TODO 记录登录数据（user_login_recode 表中加数据）

        Map<String, Object> returnMap = new HashMap<>();
        // 返回页面显示名称（优先级为 姓名 > 昵称 > 手机号）
        String name = "手机用户"+userInfo.getPhone();
        if (!StringUtils.isEmpty(userInfo.getNickName())) {
            name = userInfo.getNickName();
        }
        if (!StringUtils.isEmpty(userInfo.getName())) {
            name = userInfo.getName();
        }
        returnMap.put("name", name);

        // JWT 生成 token
        String token = JwtHelper.createToken(userInfo.getId(), name);
        // 返回登录成功的 token
        returnMap.put("token", token);

        return returnMap;
    }

    @Override
    public void userAuth(Long userId, UserAuthVo userAuthVo) {
        // 根据用户 id 获取用户信息
        UserInfo userInfo = baseMapper.selectById(userId);

        // 设置认证信息
        userInfo.setName(userAuthVo.getName());
        userInfo.setCertificatesType(userAuthVo.getCertificatesType());
        userInfo.setCertificatesNo(userAuthVo.getCertificatesNo());
        userInfo.setCertificatesUrl(userAuthVo.getCertificatesUrl());
        userInfo.setAuthStatus(AuthStatusEnum.AUTH_RUN.getStatus());

        // 进行信息更新
        baseMapper.updateById(userInfo);
    }

    @Override
    public Page<UserInfo> getPageList(Long page, Long limit, UserInfoQueryVo userInfoQueryVo) {
        System.out.println(userInfoQueryVo);

        // UserInfoQueryVo 获取条件值
        String name = userInfoQueryVo.getKeyword(); //用户名称
        Integer status = userInfoQueryVo.getStatus();//用户状态
        Integer authStatus = userInfoQueryVo.getAuthStatus(); //认证状态
        String createTimeBegin = userInfoQueryVo.getCreateTimeBegin(); //开始时间
        String createTimeEnd = userInfoQueryVo.getCreateTimeEnd(); //结束时间

        //对条件值进行非空判断
        QueryWrapper<UserInfo> wrapper = new QueryWrapper<>();
        if(!StringUtils.isEmpty(name)) {
            // 通过姓名模糊查询
            wrapper.like("name",name);
        }
        if(!StringUtils.isEmpty(status)) {
            // 状态
            wrapper.eq("status",status);
        }
        if(!StringUtils.isEmpty(authStatus)) {
            // 认证状态
            wrapper.eq("auth_status",authStatus);
        }
        if(!StringUtils.isEmpty(createTimeBegin)) {
            wrapper.ge("create_time",createTimeBegin);
        }
        if(!StringUtils.isEmpty(createTimeEnd)) {
            wrapper.le("create_time",createTimeEnd);
        }

        // 分页
        Page<UserInfo> pageParam = new Page<>(page,limit);

        // 查询
        Page<UserInfo> userInfoPage = baseMapper.selectPage(pageParam, wrapper);

        // 遍历数据，将编号变成对应的值
        userInfoPage.getRecords().forEach(this::packageUserInfo);


        return userInfoPage;
    }

    @Override
    public boolean lockById(Long userId, Integer status) {
        UserInfo userInfo = baseMapper.selectById(userId);
        userInfo.setStatus(status);
        int update = baseMapper.updateById(userInfo);

        return update == 1;
    }

    @Override
    public Map<String, Object> getUserInfoById(Long userId) {

        // 通过 userId 查询用户信息
        UserInfo userInfo = baseMapper.selectById(userId);
        // 包装一下用户信息
        this.packageUserInfo(userInfo);

        // 通过 userId 查询与之绑定的就诊人信息
        List<Patient> patientList = patientService.findAllPatientByUserId(userId);

        // 包装为 map
        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("userInfo", userInfo);
        resultMap.put("patientList", patientList);

        return resultMap;
    }

    @Override
    public boolean approvalUser(Long userId, Integer authStatus) {
        UserInfo userInfo = baseMapper.selectById(userId);
        userInfo.setAuthStatus(authStatus);
        // 更新
        int update = baseMapper.updateById(userInfo);

        return update == 1;
    }

    // 将编号变成对应的值
    private void packageUserInfo(UserInfo userInfo) {
        //处理认证状态编码
        userInfo.getParam().put("authStatusString", AuthStatusEnum.getStatusNameByStatus(userInfo.getAuthStatus()));
        //处理用户状态 0  1
        String statusString = userInfo.getStatus() == 0 ? "锁定" : "正常";
        userInfo.getParam().put("statusString", statusString);
    }
}

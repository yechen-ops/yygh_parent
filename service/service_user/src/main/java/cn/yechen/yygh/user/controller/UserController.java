package cn.yechen.yygh.user.controller;

import cn.yechen.yygh.common.result.Result;
import cn.yechen.yygh.model.user.UserInfo;
import cn.yechen.yygh.user.service.UserInfoService;
import cn.yechen.yygh.vo.user.UserInfoQueryVo;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @author yechen
 * @create 2021-08-22 16:01
 */
@Api(tags = "后台用户管理")
@RestController
@RequestMapping("/admin/user")
public class UserController {

    @Autowired
    private UserInfoService userInfoService;

    // 获取用户列表（条件查询带分页）
    @ApiOperation("获取用户列表（条件查询带分页）")
    @GetMapping("/page/{page}/{limit}")
    public Result getPageList(@PathVariable Long page, @PathVariable Long limit, UserInfoQueryVo userInfoQueryVo) {
        Page<UserInfo> userInfoPage = userInfoService.getPageList(page, limit, userInfoQueryVo);
        return Result.ok(userInfoPage);
    }

    // 用户锁定
    @ApiOperation("用户锁定 / 解锁")
    @PutMapping("/lock/{userId}/{status}")
    public Result lock(@PathVariable Long userId, @PathVariable Integer status) {
        boolean flag = userInfoService.lockById(userId, status);
        if (flag) {
            return Result.ok();
        }
        return Result.fail();
    }

    // 获取用户详情
    @ApiOperation("获取用户详情")
    @GetMapping("/showInfo/{userId}")
    public Result showInfo(@PathVariable Long userId) {
        Map<String, Object> resultMap = userInfoService.getUserInfoById(userId);

        return Result.ok(resultMap);

    }

    // 认证审批
    @ApiOperation("用户认证审批")
    @PutMapping("/approval/{userId}/{authStatus}")
    public Result approvalUser(@PathVariable Long userId, @PathVariable Integer authStatus) {
        boolean flag = userInfoService.approvalUser(userId, authStatus);
        if (flag) {
            return Result.ok();
        }
        return Result.fail();
    }


}

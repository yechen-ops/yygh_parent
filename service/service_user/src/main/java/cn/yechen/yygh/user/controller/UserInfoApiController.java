package cn.yechen.yygh.user.controller;

import cn.yechen.yygh.common.result.Result;
import cn.yechen.yygh.common.utils.AuthContextHolder;
import cn.yechen.yygh.model.user.UserInfo;
import cn.yechen.yygh.user.service.UserInfoService;
import cn.yechen.yygh.vo.user.LoginVo;
import cn.yechen.yygh.vo.user.UserAuthVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * @author yechen
 * @create 2021-08-20 17:52
 */
@Api(tags = "用户信息前台api")
@RestController
@RequestMapping("/api/user")
public class UserInfoApiController {

    @Autowired
    private UserInfoService userInfoService;

    // 手机登录加注册
    @ApiOperation("手机登录加注册")
    @PostMapping("/loginByPhone")
    public Result loginByPhone(@RequestBody LoginVo loginVo, HttpServletRequest request) {
        // TODO 获取 ip 地址
        // 进行登录
        Map<String, Object> userInfoMap = userInfoService.loginByPhone(loginVo);

        return Result.ok(userInfoMap);
    }

    // 用户实名认证接口
    @ApiOperation("用户实名认证接口")
    @PostMapping("/auth/addUserAuth")
    public Result addUserAuth(@RequestBody UserAuthVo userAuthVo, HttpServletRequest request) {
        // 传递两个参数：1、用户 id 2、认证数据的 vo 对象
        userInfoService.userAuth(AuthContextHolder.getUserId(request), userAuthVo);
        return Result.ok();
    }

    // 获取用户 id 信息接口
    @ApiOperation("获取用户 id 信息接口")
    @GetMapping("/auth/getUserInfo")
    public Result getUserInfo(HttpServletRequest request) {
        Long userId = AuthContextHolder.getUserId(request);
        UserInfo userInfo = userInfoService.getById(userId);
        return Result.ok(userInfo);
    }

}

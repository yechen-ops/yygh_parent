package cn.yechen.yygh.user.service;

import cn.yechen.yygh.model.user.UserInfo;
import cn.yechen.yygh.vo.user.LoginVo;
import cn.yechen.yygh.vo.user.UserAuthVo;
import cn.yechen.yygh.vo.user.UserInfoQueryVo;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * @author yechen
 * @create 2021-08-20 17:53
 */
public interface UserInfoService extends IService<UserInfo> {
    Map<String, Object> loginByPhone(LoginVo loginVo);

    void userAuth(Long userId, UserAuthVo userAuthVo);

    Page<UserInfo> getPageList(Long page, Long limit, UserInfoQueryVo userInfoQueryVo);

    boolean lockById(Long userId, Integer status);

    Map<String, Object> getUserInfoById(Long userId);

    boolean approvalUser(Long userId, Integer authStatus);
}

package cn.yechen.yygh.user.controller;

import cn.yechen.yygh.common.result.Result;
import cn.yechen.yygh.common.utils.AuthContextHolder;
import cn.yechen.yygh.model.user.Patient;
import cn.yechen.yygh.user.service.PatientService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.oauth2.resource.OAuth2ResourceServerProperties;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.sql.ResultSet;
import java.util.List;

/**
 * @author yechen
 * @create 2021-08-22 11:37
 */
@Api(tags = "就诊人操作api")
@RestController
@RequestMapping("/api/user/patient")
public class PatientApiController {

    @Autowired
    private PatientService patientService;

    // 添加就诊人
    @ApiOperation("添加就诊人")
    @PostMapping("/auth/savePatient")
    public Result savePatient(@RequestBody Patient patient, HttpServletRequest request) {
        // 通过 request 获取登录用户的 id
        Long userId = AuthContextHolder.getUserId(request);

        // 为 patient 对象添加登录用户 id，表示当前就诊人所属登录账号
        patient.setUserId(userId);

        // 保存就诊人
        boolean flag = patientService.savePatient(patient);

        if (flag) {
            return  Result.ok();
        }
        return Result.fail();
    }

    // 获取就诊人列表
    @ApiOperation("获取就诊人列表")
    @GetMapping("/auth/findAll")
    public Result findAllPatient(HttpServletRequest request) {
        // 通过 request 获取登录用户的 id
        Long userId = AuthContextHolder.getUserId(request);

        // 通过 userId 查询所有与之绑定的就诊人
        List<Patient> patientList = patientService.findAllPatientByUserId(userId);

        return Result.ok(patientList);
    }

    // 根据就诊人id获取就诊人信息
    @ApiOperation("根据就诊人id获取就诊人信息")
    @GetMapping("/auth/getPatient/{patientId}")
    public Result getPatientByPatientId(@PathVariable Long patientId) {
        Patient patient = patientService.getPatientByPatientId(patientId);
        return Result.ok(patient);
    }

    // 修改就诊人
    @ApiOperation("修改就诊人")
    @PutMapping("/auth/updatePatient")
    public Result updatePatient(@RequestBody Patient patient) {
        boolean flag = patientService.updatePatient(patient);
        if (flag) {
            return Result.ok();
        }
        return Result.fail();
    }

    //删除就诊人
    @ApiOperation("删除就诊人")
    @DeleteMapping("auth/removePatient/{patientId}")
    public Result removePatient(@PathVariable Long patientId) {
        boolean flag = patientService.removePatient(patientId);
        return Result.ok();
    }

}
